
package tp.neo4j.tpneo4j;

import com.mongodb.MongoClient;
import com.mongodb.MongoClientURI;
import com.mongodb.client.AggregateIterable;
import com.mongodb.client.MongoCollection;
import com.mongodb.client.MongoDatabase;
import com.mongodb.client.model.Filters;
import com.mongodb.client.model.Updates;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Scanner;
import java.util.StringTokenizer;
import org.bson.Document;
import org.neo4j.driver.v1.AuthTokens;
import org.neo4j.driver.v1.Driver;
import org.neo4j.driver.v1.GraphDatabase;
import org.neo4j.driver.v1.Record;
import org.neo4j.driver.v1.Session;
import org.neo4j.driver.v1.StatementResult;

public class App {

    //URL de connection à MongoDB
    public final static String MONGO_DB_URI = "mongodb://localhost:27017";
    
  //URL de connection à Neo4J
    public final static String NEO4J_DB_URI = "bolt://localhost:7687";

    //Nom de la base de données MongoDB
    public final static String MONGO_DB_BASE = "dbDocuments";

    //Collection MongoDB qui contient les mots-clés
    public final static String MONGO_DB_COLLECTION_INDEX = "index";

    //Collection MongoDB qui contient les mots-clés inverse
    public final static String MONGO_DB_COLLECTION_MIROIR = "indexInverse";

    //Clavier pour récupérer les réponses de l'utilisateur
    public final static Scanner saisie = new Scanner(System.in);
    
    public static void main(String[] args) {
        boolean actions_menu = true;
        String reponse;
        // Session pour se connecter à Neo4J
        Session session = null;

        try (MongoClient mongoClient = new MongoClient(new MongoClientURI(MONGO_DB_URI));
                Driver neo4jClient = GraphDatabase.driver(NEO4J_DB_URI, AuthTokens.basic("neo4j", "root"))) {
            while (actions_menu) {
            	affiche_menu();
                reponse = getReponseUtilisateur("Veuillez sélectionner un item : ");
                System.out.println();
                session = neo4jClient.session();
                switch (reponse) {
                    case "1":
                        createDataBase(mongoClient, session);
                        break;
                    case "2":
                        creer_structure_miroir(mongoClient);
                        break;
                    case "3":
                    	recherche_document(mongoClient, session);
                        break;
                    case "4":
                    	ListAuthorNbArticles(session);
                        break;
                    case "5":
                    	recherche_avancee(mongoClient, session);
                    	break;
                    case "Q":
                    	actions_menu = false;
                        break;
                }
                session.close();
            }
        }
    }

    //Mettre en place un datastore MongoDB
    public static void createDataBase(MongoClient mongoClient, Session session) {
        
        /* Neo4J */
        StatementResult resultat; 
        Record rec;                
        StringTokenizer token;   
        String[] motsCles;      

        /* MongoBD */
        MongoDatabase mongoDB;           
        MongoCollection<Document> mongoCollection; 
        Document document;
        
        String reponse;
        boolean est_cree = true;

        if (est_cree) {
            System.out.println("Mise à jour des mots-clés...");
            resultat = session.run("MATCH (a :Article) RETURN a.titre AS titre, ID(a) AS id");

            if (!resultat.hasNext()) {
                System.out.println("Aucun article existe dans Neo4J !");
            } else {
                mongoDB = mongoClient.getDatabase(MONGO_DB_BASE);
                mongoDB.drop();
                mongoCollection = mongoDB.getCollection(MONGO_DB_COLLECTION_INDEX);
                while (resultat.hasNext()) {
                    rec = resultat.next();
                    if (!rec.get("titre").isNull() && !rec.get("id").isNull()) {
                        token = new StringTokenizer(rec.get("titre").asString().toLowerCase(),
                                " -:;.()+[]{}?'=");
                        motsCles = string_Array(token);
                        document = Document.parse("{idDocument : " + rec.get("id").asInt()
                                + ",motsCles : " + Arrays.toString(motsCles) + "}");
                        mongoCollection.insertOne(document);
                    }
                }
                System.out.println("Les motés-clés ont été mises à jour");
                System.out.println(mongoCollection.count() + " documents ont été ajoutés");
                mongoCollection.createIndex(Document.parse("{motsCles : 1}"));
                System.out.println("Index créé sur les mots clés");
            }
        }
        
        if (isCollectionExist(mongoClient, MONGO_DB_BASE, MONGO_DB_COLLECTION_INDEX)) {
            System.out.println("La collection MongoDB '" + MONGO_DB_COLLECTION_INDEX + "' existe déjà");
            reponse = getReponseUtilisateur("Voulez-vous supprimer la collection existantes (O/N) ? ");
            est_cree = reponse.toUpperCase().equals("Y");
        }
    }

    //Mettre en place une structure miroir sur MongoDB </p>
    public static void creer_structure_miroir(MongoClient mongoClient) {
        String reponse;
        boolean is_create = true;
        
        /* MongoBD */
        MongoDatabase mongoDB;                  
        MongoCollection<Document> index;
        MongoCollection<Document> miroir;

        if (isCollectionExist(mongoClient, MONGO_DB_BASE, MONGO_DB_COLLECTION_MIROIR)) {
            System.out.println("La Collection MongoDB '" + MONGO_DB_COLLECTION_MIROIR + "' existe déjà !");
            reponse = getReponseUtilisateur("Voulez-vous supprimer la collection existantes (O/N) ? ");
            is_create = reponse.toUpperCase().equals("O");
        }

        if (is_create) {
            System.out.println("Mise à jour de la structure miroir...");
            mongoDB = mongoClient.getDatabase(MONGO_DB_BASE);
            index = mongoDB.getCollection(MONGO_DB_COLLECTION_INDEX);
            miroir = mongoDB.getCollection(MONGO_DB_COLLECTION_MIROIR);
            // On supprime la collection miroir
            miroir.drop(); 

            for (Document doc : index.find()) {
                for (String motCle : (List<String>) doc.get("motsCles")) {
                    Document d = miroir.find(Filters.eq("mot", motCle)).first();
                    if (d == null) {
                        miroir.insertOne(Document.parse("{mot: \"" + motCle
                                + "\", documents : [" + doc.getInteger("idDocument") + "]}"));
                    } else {
                        miroir.updateOne(Filters.eq("_id", d.get("_id")),
                                Updates.addToSet("documents", doc.getInteger("idDocument")));
                    }
                }
            }

            System.out.println("La structure miroir a été mise à jour");
            System.out.println(miroir.count() + " documents ont été ajoutés");
            miroir.createIndex(Document.parse("{documents : 1}"));
            System.out.println("Index créé sur la structure miroir");
        }
    }
    
    public static List<String> compute_string(String chaine){
        ArrayList<String> tags = new ArrayList();
        String chaineEnMinuscule = chaine.toLowerCase() ;
        // Découpe en fonction des caractères , ou .
        StringTokenizer st = new StringTokenizer (chaineEnMinuscule, ",'-:;.()+[]{}?! ");
        while (st.hasMoreTokens()){
            // Récupération du mot suivant
            String mot = st.nextToken();
            tags.add(mot.trim());
            // Traite le mot…..
        }
        return tags;
    }
    
    public static void recherche_avancee(MongoClient mongoClient, Session session) {
    	System.out.println("\nEntrez vos mots séparés par des espaces : ");
        Scanner mots_saisis = new Scanner(System.in);
        //Permet de découper la liste de mots
        List<String> words = compute_string(mots_saisis.nextLine());  
        
        //Requête mongoDB
        AggregateIterable<Document> aggregDocuments = mongoClient.getDatabase(MONGO_DB_BASE).getCollection("indexIverse").aggregate(Arrays.asList(
                new Document("$match",
                    new Document("mot",
                        new Document("$in",words))),
                new Document("$unwind", "$documents"),
                
                new Document("$group",
                    new Document(
                            new HashMap()
                                {{
                                    put("_id", "$documents");
                                    put("count",new Document("$sum",1));
                                }}
                    )),
                
                new Document("$sort",
                    new Document("count", -1)),
                
                new Document("$limit",10)
        ));

        System.out.println("\nRésultats de la recherche avancée : ");
        
        // Pour chaque résultat dans MongoDB
        for(Document d : aggregDocuments) {
            String req = "match (a:Article)\n" +
                    "where id(a) = "+ d.get("_id").toString() +"\n" +
                    "return a.titre\n" +
                    "order by a.titre";
            StatementResult result = session.run(req);
            System.out.println(d.get("_id").toString()+ " " + result.single().get("a.titre").asString()+" "+d.get("count").toString());
        }
    }

    //Recherche un mot-clé dans le la structure miroir de MongoDB et on affiche les titres des documents qui contiennent ce mot-clé</p>
    public static void recherche_document(MongoClient mongoClient, Session session) {       
        //Version MongoBD
        MongoDatabase mongoDB;
        MongoCollection<Document> mongoMiroir;
        Document document; 
        
        String motCle;

        if (!isCollectionExist(mongoClient, MONGO_DB_BASE, MONGO_DB_COLLECTION_MIROIR)) {
            System.out.println("La structure miroir n'a pas été créé !");
        } else {
            motCle = getReponseUtilisateur("Saisissez le mot clé que vous souhaitez rechercher ");
            mongoDB = mongoClient.getDatabase(MONGO_DB_BASE);
            mongoMiroir = mongoDB.getCollection(MONGO_DB_COLLECTION_INDEX);
            document = mongoMiroir.find(Filters.eq("mot", motCle)).first();
            
            if (document == null) {
                System.out.println("Le mot-clé '" + motCle + " n'existe pas");
            }
        }
    }
    
    //Fonction permettant d'afficher les auteurs et le nombres d'articles qu'ils ont rédigé
  	public static void ListAuthorNbArticles(Session session) {
  		StatementResult result = session.run("match (a:Article) <- [ECRIRE] - (au:Auteur) return count(a) as nbArticles, au order by nbArticles DESC");
  		//Récupération du résultat de la requête
  		while(result.hasNext()) {
  			Record record = result.next();
  			System.out.println(record.get("nbArticles").toString() + " - " + record.get("au").get("nom").toString());
  		}
  	}
    
    //Affiche le menu
    public static final void affiche_menu() {
        System.out.println("\nMenu des actions : ");
        System.out.println("\t1 - Mettre à jour la collection des articles sur MongoDB");
        System.out.println("\t2 - Mettre en place la structure miroir sur MongoDB");
        System.out.println("\t3 - Recherche tous les documents contenant un mot-clé");
        System.out.println("\t4 - Afficher au plus 5 films proches");
        System.out.println("\t5 - Recherche avancée");
        System.out.println("\t" + "Q" + " - Quitter le programme");
    }

    //Affiche un message sur la console et demande à l'utilisateur d'y répondre.
    public static String getReponseUtilisateur(String message) {
        System.out.print("Vous avez choisit : " + message);
        return saisie.nextLine();
    }

    //Transforme une StringTokenizer en un tableau de chaine de caratères
    public static String[] string_Array(StringTokenizer token) {
        String[] array = new String[token.countTokens()];
        for (int i = 0, n = array.length; i < n; i++) {
            array[i] = "\"" + token.nextToken().trim() + "\"";
        }
        return array;
    }
    
    //Vérifie si la collection MongoBD existe déjà
    public static boolean isCollectionExist(MongoClient mongoClient,
            String nomBD, String nomCollection) {
        for (String bdName : mongoClient.listDatabaseNames()) {
            if (bdName.equals(nomBD)) { // On a reconnu la base de données dans MongoDB
                for (String collectionName : mongoClient.getDatabase(nomBD).listCollectionNames()) {
                    if (collectionName.equals(nomCollection)) { // On a reconnu la collection dans MongoDB
                        return true;
                    }
                }
            }
        }
        return false;
    }
}
